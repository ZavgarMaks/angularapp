var express = require('express');
var controller = require('./news.controller');
import * as auth from '../../auth/auth.service';


var router = express.Router();

router.post('/', auth.isAuthenticated(),controller.createNews);
router.get('/',controller.showAllNews);
router.get('/me',auth.isAuthenticated(), controller.showPersonalNews);
router.get('/:id',auth.isAuthenticated(), controller.showNewsNeedUser);
router.delete('/:id',auth.isAuthenticated(),controller.removeOneNews);

module.exports = router;
