import mongoose from 'mongoose';

var newsSchema = mongoose.Schema({
  text: String,
  topic: String,
  author: String,
  creator: String,
  date: String,
  tags: [{type:mongoose.Schema.Types.ObjectId, ref: 'Tags'}]
});

var News = mongoose.model('News', newsSchema);

export default mongoose.model('News', newsSchema);

