import News from './news.model';
import Tags from '../tags/tags.model'
// import User from '../user/user.model';


export function createNews(req,res) {

  var tagsId = [];
  console.log('2',req.body.tags.map(function(item) {
    var tagVal = [];
    return Tags.findOne({name:item},function(err, tags) {
      // console.log('err1',tags);
      if (err) {
        res.status(500).send(err);
        return console.error('!!!!!!!!!!!!!!!!!!', err);
      }
      if (tags) {
        tagVal = tags._id;
        console.log('111111111', tagVal);
        return tagVal;
      }
      return tagVal;
    })
  }));
  // var tagsArray = req.body.tags.map(function(item) {
  //   var tagVal;
  //   Tags.findOne({name:item},function(err, tags) {
  //     // console.log('err1',tags);
  //     if (err) {
  //       res.status(500).send(err);
  //       return console.error('!!!!!!!!!!!!!!!!!!', err);
  //     }
  //     if (tags) {
  //       tagVal = tags._id;
  //       console.log('111111111', tagVal);
  //       return tagVal;
  //     }
  //   });
    // }).then(()=> {
    //   if (tagVal) {
    //     tagsId.push(tagVal);
    //     return tagsId
    //   } else {
    //     var newTag = new Tags ({
    //       name:item
    //     });
    //     newTag.save(function (err, tags) {
    //         console.log('err2',tags);
    //         if (err){
    //           res.status(500).send(err);
    //           return console.error(err);
    //         }
    //     });
    //     tagsId.push(newTag._id)
    //   }
    // }).then(()=> {
    //   console.log('2',tagsId);
    //   return tagsId
    // });

  // });
  // console.log('--------->',tagsArray)
  // Promise.all(tagsArray).then(()=> {
  //   console.log('--------->',tagsId)
  //   var newNews = new News({
  //     text: req.body.text,
  //     topic: req.body.topic,
  //     author: req.body.author,
  //     creator:req.user._id,
  //     date:req.body.date,
  //     tags:tagsId,
  //   });
  //   newNews.save(function (err, news) {
  //     console.log('err',news);
  //     if (err){
  //       res.status(500).send(err);
  //       return console.error(err);
  //     }
  //     res.send(news);
  //   })
  // });
  //


}

export function showAllNews(req,res) {
  return News.find(function(err, news) {
    if (err){
      res.status(500).send(err);
      return console.error(err);
    }
    res.send(news);
  })
}

export function showPersonalNews(req,res) {
  return News.find({creator:req.user._id},function(err, news) {
    if (err){
      res.status(500).send(err);
      return console.error(err);
    }
    res.send(news);
  })
}
export function showNewsNeedUser(req,res) {
  console.log(req.params.id);
  return News.find({creator:req.params.id},function(err, news) {
    if (err){
      res.status(500).send(err);
      return console.error(err);
    }
    res.send(news);
  })
}
export function removeOneNews(req,res) {
  console.log(req.params.id)
  News.remove({_id:req.params.id},function(err, news) {
    if (err){
      res.status(500).send(err);
      return console.error(err);
    }
    res.send(news);
  })
}
