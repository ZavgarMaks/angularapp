import mongoose from 'mongoose';
var tagsSchema = mongoose.Schema({
  name:String,
});
var Tags = mongoose.model('Tags', tagsSchema);

export default mongoose.model('Tags', tagsSchema);
