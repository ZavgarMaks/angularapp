
import angular from 'angular';
import ProfileController from '../../app/profile/profile.component'

export default angular.module('angularApp.publishNews', [])
  .component('publishNews', {
    template: require('./publishNews.html'),
    controller: ProfileController
  })
  .name;
