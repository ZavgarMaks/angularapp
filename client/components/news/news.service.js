import _ from 'lodash';




export default function NewsService($http) {
  'ngInject';

  var News = {
    addNews(text,topic,author,dateNow,tagsList) {
      console.log(text,topic,author,dateNow);
      return $http({
        method:"POST",
        url:"/api/news/",
        data: {text:text,author:author,topic:topic, date: dateNow, tags:tagsList}
      }).then(()=>{
        return $http({
          method:"GET",
          url: "/api/news/me/",
        })
      })
    },
    showPersonalNews(author) {
      return $http({
        method:"GET",
        url: "/api/news/me/",
      });

    },
    getAllTags() {
      return $http({
        method:"GET",
        url: "/api/tags/",
      }).then((response)=>{
        return response.data})
    },
    showNewsNeedUser(id) {
      return $http({
        method:"GET",
        url: "/api/news/"+id,
      });

    },
    showAllNews() {
      return $http({
        method:"GET",
        url: "/api/news/",
      }).then((response)=>{
           return response.data})
    },
    removeOneNews(_id){
      var id = _id;
      return $http({
        method:"DELETE",
        url:"/api/news/"+id
      }).then(()=>{
        return $http({
          method:"GET",
          url: "/api/news/me/",
        })
      })
    },
  };

  return News;
}


