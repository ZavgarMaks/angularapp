'use strict';
export default class InfoController {
  user = {
    name:'',
    email:'',
    age:'',
    address:'',
  };
  submitted = false;


  /*@ngInject*/
  constructor(Auth) {
    'ngInject';
    this.getCurrentUser = Auth.getCurrentUserSync;
    this.Auth = Auth;

  }
  $onInit () {
    this.user.name = this.getCurrentUser().name;
    this.user.email = this.getCurrentUser().email;
    this.user.age = this.getCurrentUser().age;
    this.user.address = this.getCurrentUser().address;
  }
  editInfo(form) {
    this.submitted = true;
    if (form.$valid) {
      this.Auth.editInfo(this.user.name,this.user.email,this.user.age,this.user.address);
      document.location.href = "http://localhost:3000/profile";
    }
  }

}
