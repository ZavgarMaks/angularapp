import angular from 'angular';
import InfoController from './infoedit.controller';

export default angular.module('angularApp.infoedit', [])
  .controller('InfoController', InfoController)
  .name;
