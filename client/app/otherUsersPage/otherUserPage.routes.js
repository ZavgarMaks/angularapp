'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('otherUserPage', {
    url: '/otherUserPage/:id',
    component: 'otherUserPage',
    resolve:{
      thisUser: getUser,
      latestNews: getNewsForCurrentUser
    }
  });
}


function getUser($http,$stateParams){
  'ngInject';

  var id = $stateParams.id;
  return $http({
    method:"GET",
    url: "/api/users/"+id ,
  }).then((response)=> {
    return response.data;

  })
}

function getNewsForCurrentUser(NewsService,$stateParams){
  'ngInject';

  var id = $stateParams.id;
  return NewsService.showNewsNeedUser(id).then((response)=>{
    return response.data;
  })
}
