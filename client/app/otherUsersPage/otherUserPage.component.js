import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './otherUserPage.routes';



export default angular.module('angularApp.otherUserPage', [uiRouter])
  .config(routing)
  .component('otherUserPage', {
    bindings:{
      thisUser:'<',
      latestNews:'<'
    },
    template: require('./otherUserPage.html'),

  })
  .name;
