/**
 * Created by dunice on 24.04.17.
 */
'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('profile', {
    url: '/profile',
    component: 'profile',
    resolve:{
      currentUser: getCurrentUser,
      latestNews: getNewsForCurrentUser,
      // allTags: getTags,
    }
  });
}


function getCurrentUser(Auth){
  'ngInject';
  return Auth.getCurrentUser();

}

function getNewsForCurrentUser(NewsService,){
  'ngInject';

  return NewsService.showPersonalNews().then((response)=>{
    return response.data;
  })
}
// function getTags(NewsService) {
//   'ngInject';
//   return NewsService.getAllTags()
// }

