import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './profile.routes';

export class ProfileController {

  user = {
    text:'',
    topic:'',
    date: '',
    tags: '',
  };
  submitted = false;
  latestNews = [];


  constructor(NewsService,Auth) {
    'ngInject';
    this.NewsService = NewsService;
    this.Auth = Auth
  }


  addNews() {
    var author = this.currentUser.name;
    var options = {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric',
      timezone: 'UTC'
    };
    var tagsList = this.user.tags.split(' ');
    console.log(tagsList);
    var dateNow = new Date().toLocaleString("ru", options);
    this.submitted = true;
      this.NewsService.addNews(this.user.text,this.user.topic,author, dateNow, tagsList).then((response) => {
        this.latestNews = response.data;
        return response.data
      });
    this.user = {
      text:'',
      topic:''
    };
  }


  deleteOneNews(_id) {
    this.NewsService.removeOneNews(_id).then((response) => {
      this.latestNews = response.data;
      return response.data
    });
  }



}
export default angular.module('angularApp.profile', [uiRouter])
  .config(routing)
  .component('profile', {
    bindings:{
      currentUser:'<',
      latestNews:'<',
      allTags:'<'
    },
    template: require('./profile.html'),
    controller: ProfileController,
    controllerAs:'vm'
  })
  .name;
