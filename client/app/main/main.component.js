import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './main.routes';

export class MainController {

  user = {
    text:'',
    topic:''
  };
  allNews = [];
  submitted = false;
  /*@ngInject*/
  constructor(Auth,NewsService,$state) {
    'ngInject';
    this.getCurrentUser = Auth.getCurrentUserSync;
    this.Auth = Auth;
    this.NewsService = NewsService;
    console.log(Date())
  }

}

export default angular.module('angularApp.main', [uiRouter])
  .config(routing)
  .component('main', {
    bindings:{
      allNews:'<'
    },
    template: require('./main.html'),
    controller: MainController
  })
  .name;
