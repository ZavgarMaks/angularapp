'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('main', {
    url: '/',
    component:'main',
    resolve: {
      allNews: getAllNews
    }
  });
}

function getAllNews(NewsService){
  'ngInject';

  return NewsService.showAllNews()
  }

